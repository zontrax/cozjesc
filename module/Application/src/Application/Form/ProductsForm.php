<?php
/**
 * Created by PhpStorm.
 * User: RoberBułat
 * Date: 07.09.2018
 * Time: 11:08
 */

namespace Application\Form;

use Zend\Form\Form;

class ProductsForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('products');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'product_name',
            'attributes' => array(
                'type'  => 'text',
                'required' => true,
                'placeholder' => 'Nazwa produktu',
                'class' => 'form-control',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_type',
            'options' => array(
                'empty_option' => 'Wybierz typ produktu',
                'value_options' => array(
                    '0' => 'Produkty zbożowe',
                    '1' => 'Mleko i przetwory mleczne',
                    '2' => 'Mięso, wędliny, ryby i podroby',
                    '3' => 'Jaja',
                    '4' => 'Masło i śmietana',
                    '5' => 'Inne tłuszcze',
                    '6' => 'Ziemniaki',
                    '7' => 'Warzywa i owoce',
                    '8' => 'Suche nasiona strączkowe',
                    '9' => 'Cukier i słodycze'
                ),
            ),
            'attributes' => array(
                'required' => true,
                'placeholder' => 'Typ produktu',
                'class' => 'form-control',
    )
        ));

        $this->add(array(
            'name' => 'product_energy_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Energia w 100g (kcal)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'product_energy_1_piece',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Energia w 1 sztuce (kcal)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'protein_in_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość białka w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'protein_in_1_piece',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość białka w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'fat_in_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość tłuszczu w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'fat_in_1_piece',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość tłuszczu w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'saturated_fatty_acids_in_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość KTN w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'saturated_fatty_acids_in_1_piece',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość KTN w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'carbohydrates_in_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość węglowodanów w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'carbohydrates_in_1_piece',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość węglowodanów w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'cellulose_in_100g',
            'attributes' => array(
                'type'  => 'value',
                'required' => true,
                'placeholder' => 'Zawartość błonnika w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'cellulose_in_1_piece',
            'attributes' => array(
                'type'  => 'text',
                'required' => true,
                'placeholder' => 'Zawartość błonnika w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'salt_in_100g',
            'attributes' => array(
                'type'  => 'text',
                'required' => true,
                'placeholder' => 'Zawartość soli w 100g (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'salt_in_1_piece',
            'attributes' => array(
                'type'  => 'text',
                'required' => true,
                'placeholder' => 'Zawartość soli w 1 sztuce (g)',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
                'required' => true,
                'placeholder' => 'Opis',
                'class' => 'form-control',
            ),
        ));
        
        $this->add(array(
            'name' => 'unique_id',
            'attributes' => array(
                'type'  => 'hidden',
                'id' => 'unique_id',
            ),
        ));

        $this->add(array(
            'name' => 'submitproduct',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Dodaj produkt',
                'id' => 'submitproduct',
                'class' => 'btn btn-block btn-info'
            ),
        ));
    }
}