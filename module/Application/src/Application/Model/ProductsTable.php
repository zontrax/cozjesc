<?php
/**
 * Created by PhpStorm.
 * User: RoberBułat
 * Date: 06.09.2018
 * Time: 18:48
 */

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;


class ProductsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveUser(Auth $auth)
    {
        // for Zend\Db\TableGateway\TableGateway we need the data in array not object
        $data = array(
            'name' => $auth->name,
            'lastname' => $auth->lastname,
            'usr_name' => $auth->usr_name,
            'usr_password' => $auth->usr_password,
            'usr_email' => $auth->usr_email,
            'usrl_id' => $auth->usrl_id,
            'lng_id' => $auth->lng_id,
            'usr_active' => $auth->usr_active,
            'usr_question' => $auth->usr_question,
            'usr_answer' => $auth->usr_answer,
            'usr_picture' => $auth->usr_picture,
            'usr_password_salt' => $auth->usr_password_salt,
            'usr_registration_date' => $auth->usr_registration_date,
            'usr_registration_token' => $auth->usr_registration_token,
            'passwordchangetoken' => $auth->passwordchangetoken,
            'usr_email_confirmed' => $auth->usr_email_confirmed,
            'usr_passwordchangetoken_active' => $auth->usr_passwordchangetoken_active
        );

    }
}