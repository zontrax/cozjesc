<?php
use Zend\Form\Element;
use Zend\Form\Form;

class UploadForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->addElements();
    }
    
    public function addElements()
    {
        // File Input
        $file = new Element\File('image-file');
        $file->setLabel('Avatar Image Upload')
        ->setAttribute('id', 'multiple_file');
        ->setAttribute('name', 'multiple_file');
        ->setAttribute('multiple', 'true');
        $this->add($file);
    }
}