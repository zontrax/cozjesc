<?php
namespace Application\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
// the object will be hydrated by Zend\Db\TableGateway\TableGateway
class Products implements InputFilterAwareInterface
{

    public $product_id;
    public $product_name;
    public $product_type;
    public $product_replacement;
    public $product_energy_100g;
    public $product_energy_1_piece;
    public $protein_in_100g;
    public $protein_in_1_piece;
    public $fat_in_100g;
    public $fat_in_1_piece;
    public $saturated_fatty_acids_in_100g;
    public $saturated_fatty_acids_in_1_piece;
    public $carbohydrates_in_100g;
    public $carbohydrates_in_1_piece;
    public $cellulose_in_100_g;
    public $cellulose_in_1_piece;
    public $salt_in_100g;
    public $salt_in_1_piece;
    public $photos;
    public $description;
    public $approved

    // Hydration
    // ArrayObject, or at least implement exchangeArray. For Zend\Db\ResultSet\ResultSet to work
    public function exchangeArray($data)
    {
        $this->product_id = (!empty($data['product_id'])) ? $data['product_id'] : null;
        $this->product_name = (!empty($data['product_name'])) ? $data['product_name'] : null;
        $this->product_type = (!empty($data['product_type'])) ? $data['product_type'] : null;
        $this->product_replacement = (!empty($data['product_replacement'])) ? $data['product_replacement'] : null;
        $this->product_energy_100g = (!empty($data['product_energy_100g'])) ? $data['product_energy_100g'] : null;
        $this->product_energy_1_piece = (!empty($data['product_energy_1_piece'])) ? $data['product_energy_1_piece'] : null;
        $this->protein_in_100g = (!empty($data['protein_in_100g'])) ? $data['protein_in_100g'] : null;
        $this->protein_in_1_piece = (!empty($data['protein_in_1_piece'])) ? $data['protein_in_1_piece'] : null;
        $this->fat_in_100g = (isset($data['fat_in_100g'])) ? $data['fat_in_100g'] : null;
        $this->fat_in_1_piece = (!empty($data['fat_in_1_piece'])) ? $data['fat_in_1_piece'] : null;
        $this->saturated_fatty_acids_in_100g = (!empty($data['saturated_fatty_acids_in_100g'])) ? $data['saturated_fatty_acids_in_100g'] : null;
        $this->saturated_fatty_acids_in_1_piece = (!empty($data['saturated_fatty_acids_in_1_piece'])) ? $data['saturated_fatty_acids_in_1_piece'] : null;
        $this->carbohydrates_in_100g = (!empty($data['carbohydrates_in_100g'])) ? $data['carbohydrates_in_100g'] : null;
        $this->carbohydrates_in_1_piece = (!empty($data['carbohydrates_in_1_piece'])) ? $data['carbohydrates_in_1_piece'] : null;
        $this->cellulose_in_100_g = (!empty($data['cellulose_in_100_g'])) ? $data['cellulose_in_100_g'] : null;
        $this->cellulose_in_1_piece = (!empty($data['cellulose_in_1_piece'])) ? $data['cellulose_in_1_piece'] : null;
        $this->salt_in_100g = (isset($data['salt_in_100g'])) ? $data['salt_in_100g'] : null;
        $this->salt_in_1_piece = (isset($data['salt_in_1_piece'])) ? $data['salt_in_1_piece'] : null;
        $this->photos = (isset($data['photos'])) ? $data['photos'] : null;
        $this->description = (isset($data['description'])) ? $data['description'] : null;
        $this->approved = (isset($data['approved'])) ? $data['approved'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


    protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'usr_name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'usr_password',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}