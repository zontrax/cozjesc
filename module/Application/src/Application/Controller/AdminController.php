<?php
namespace Application\Controller;

use Application\Form\ProductsForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Lists\Entity\Lists;
use Zend\Session\Container;


use Zend\Db\TableGateway\TableGateway;
use Zend\File\Transfer\Adapter;

use Application\Form\UserForm;
use Application\Form\UserFilter;
use Zend\Form\Element\File;
use Zend\Db\Sql\Ddl\Column\Date;


class AdminController extends AbstractActionController
{
	protected $usersTable = null;
    protected $productsTable = null;
    protected $productsImagesTable = null;

	// R - retrieve = Index
    public function indexAction()
    { 
		return new ViewModel(array('rowset' => $this->getProductsTable()->select()));
	}

	public function createProductAction()
    {       
        session_start();
        $_SESSION["unique_id"] = bin2hex(random_bytes(32));
        $unique_id = $_SESSION["unique_id"];

        $unique_count = $this->getProductsImagesTable()->select(array('item_id' => $unique_id));
        $unique_id_count = $unique_count->count();
                
        while ($unique_id_count >0)
        {
            $_SESSION["unique_id"] = bin2hex(random_bytes(32));
            $unique_id = $_SESSION["unique_id"];

            $unique_count = $this->getProductsImagesTable()->select(array('item_id' => $unique_id));
            $unique_id_count = $unique_count->count();
        }
        
        session_write_close();
        
        
        $form = new ProductsForm();
        $form->get('unique_id')->setValue($unique_id);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
/*            $form->setInputFilter(new UserFilter());*/
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                $unique_value = $form->get('unique_id')->getValue();
                                
                $data['photos_id'] = $unique_value;
                unset($data['unique_id']);
                unset($data['submitproduct']);
                $date = date('Y-m-d H:i:s');
                $this->getProductsTable()->insert($data);
                $this->getProductsImagesTable()->update(array(added_time => $date, 'added' => '1'), array('item_id = ?' => $unique_value));
                
                return $this->redirect()->toRoute('application/default', array('controller' => 'admin', 'action' => 'index'));
            }
        }
        

//        $upload = new Image();
//        $files  = $upload->getFileInfo();
//        foreach($files as $file => $fileInfo) {
//            if ($upload->isUploaded($file)) {
//                if ($upload->isValid($file)) {
//                    if ($upload->receive($file)) {
//                        $info = $upload->getFileInfo($file);
//                        $tmp  = $info[$file]['tmp_name'];
//                        // here $tmp is the location of the uploaded file on the server
//                        // var_dump($info); to see all the fields you can use
//                    }
//                }
//            }
//        }
        return new ViewModel(array('form' => $form));
    }
    
    public function fetchImagesAction()
    {
        session_start();
        $result = $this->getProductsImagesTable()->select(array('item_id' => $_SESSION['unique_id']));
        $numberofrows = $result->count();
        
        $result = $result->toArray();
        
        $uri = $this->getRequest()->getUri();
        $baseUrl = sprintf('%s://%s/', $uri->getScheme(), $uri->getHost());
        
        $output = '
        ';
        
        if ($numberofrows > 0)
        {
            $count = 0;
            foreach($result as $row)
            {
                $count = $count +1;
                $output .= '
                <div class="col-sm-3">
                <tr>
                <br>
                    <img src="' . $baseUrl . 'image-products/'.$row["image_name"].'" class="img-thumbnail" width="150" height="150" />
                    <br>
                    <button type="button" class="btn btn-warning btn-xs edit" id="'.$row["product_image_id"].'">Edytuj</button><button type="button" class="btn btn-danger btn-xs delete" id="'.$row["product_image_id"].'" data-image-name="'.$row["image_name"].'">Usuń</button></td>
                </tr>
                <br>
                </div>                
                ';
            }
            
        }
        else
        {
            $output .= '
            <tr>
                <td colspan="6" align="center"> Nie wybrano zdjęć.' . '</td> 
            </tr>
            ';
        }
        
        $output .='</table>';
        echo $output;
        $view =  new ViewModel();
        $view->setTerminal(true);
        return $view;
        session_write_close();
    }
    
    public function uploadImagesAction()
    {
        session_start();
        $unique_session = $_SESSION["unique_id"];


        if(count($_FILES["file"]["name"]) > 0)
        {
            for($count=0; count($_FILES["file"]["name"]); $count++)
            {
                $tmp_name = $_FILES["file"]["tmp_name"][$count];

                $file_name = $_FILES["file"]["name"][$count];
                $file_array = explode(".", $file_name);
                $file_extension = end($file_array);


                $unique_filename_count = 1;
                while ($unique_filename_count >0)
                {
                    $file_name = $file_array[0] . '-' . rand() . '.' . $file_extension;

                    $unique_filename1 = $this->getProductsImagesTable()->select(array('image_name' => $file_name));
                    $unique_filename_count = $unique_filename1->count();
                }

                $location = 'public/image-products/' . $file_name;

                if(move_uploaded_file($tmp_name, $location))
                {
                    $this->getProductsImagesTable()->insert(array('image_name' => $imagealreadycount . $file_name, 'item_id' => $unique_session));

                }
            }
        }
    }

    private function file_already_uploaded($file_name)
    {
        $imagealready = $this->getProductsImagesTable()->select(array('image_name' => $file_name));
        $imagealreadycount = $imagealready->count();
        return $imagealreadycount;
    }

	public function editImageAction()
    {
        $result = $this->getProductsImagesTable()->select(array('image_name' => $_POST["image_name"]));
        $resultcount = $result->count();
        $result->toArray();
        var_dump($resultcount);
//        foreach($result as $row)
//        {
//            $file_array = explode(".", $row["image_name"]);
//            $output['image_name'] = $file_array[0];
//            $output['image_description'] = $row['image_description'];
//        }

        return new JsonModel(array('image_data' => $result));

    }

	
	public function getUsersTable()
	{
		// I have a Table data Gateway ready to go right out of the box
		if (!$this->usersTable) {
			$this->usersTable = new TableGateway(
				'users', 
				$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
//				new \Zend\Db\TableGateway\Feature\RowGatewayFeature('usr_id') // Zend\Db\RowGateway\RowGateway Object
//				ResultSetPrototype
			);
		}
		return $this->usersTable;
	}

    public function getProductsTable()
    {
        // I have a Table data Gateway ready to go right out of the box
        if (!$this->productsTable) {
            $this->productsTable = new TableGateway(
                'products',
                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
//				new \Zend\Db\TableGateway\Feature\RowGatewayFeature('usr_id') // Zend\Db\RowGateway\RowGateway Object
//				ResultSetPrototype
            );
        }
        return $this->productsTable;
    }
    
        public function getProductsImagesTable()
    {
        // I have a Table data Gateway ready to go right out of the box
        if (!$this->productsImagesTable) {
            $this->productsImagesTable = new TableGateway(
                'products-images',
                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
//				new \Zend\Db\TableGateway\Feature\RowGatewayFeature('usr_id') // Zend\Db\RowGateway\RowGateway Object
//				ResultSetPrototype
            );
        }
        return $this->productsImagesTable;
    }
}